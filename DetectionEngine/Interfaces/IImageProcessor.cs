﻿using Emgu.CV;

namespace DetectionEngine.Interfaces
{
    public interface IImageProcessor
    {
        void ProcessImage(IImage image);
    }
}
