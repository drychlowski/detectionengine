﻿using System.Drawing;
using Emgu.CV;

namespace DetectionEngine.Interfaces
{
    public interface IImageLoader
    {
        IImage LoadFromFile(string path);
        IImage LoadFromBitmap(Bitmap bitmap);
    }
}
