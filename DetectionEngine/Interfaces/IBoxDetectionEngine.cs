﻿using System.Collections.Generic;
using System.Drawing;
using Emgu.CV;

namespace DetectionEngine.Interfaces
{
    public interface IBoxDetectionEngine
    {
        IEnumerable<Bitmap> GetMatchingBoxes(IImage image);
    }
}
