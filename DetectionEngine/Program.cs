﻿using DetectionEngine.Engine;
using Emgu.CV;
using Emgu.CV.Structure;

namespace DetectionEngine
{
    public class Program
    {
        static void Main(string[] args)
        {

            var imageLoader = new ImageLoader();
            var image = imageLoader.LoadFromFile("img\\src.jpg");

            var predefImageProcessors = new PredefDefaultImageProcessors();

            var detectionEngine = new BoxDetectionEngine(predefImageProcessors.GetImageProcessors(), new DefaultBoxProcessor());
            var boxes = detectionEngine.GetMatchingBoxes(image);

            
            foreach (var box in boxes)
            {
                var img = new Image<Bgr, byte>(box);
                img.Save($"{img.Size}.jpg");
            }
            
        }
    }
}
