﻿using System;
using System.Collections.Generic;
using System.Drawing;
using DetectionEngine.Interfaces;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;

namespace DetectionEngine.Engine
{
    public class BoxDetectionEngine : IBoxDetectionEngine
    {
        private readonly IEnumerable<IImageProcessor> _imageProcessors;
        private readonly IBoxProcessor _boxProcessor;
        private readonly List<Bitmap> _detectedBoxes;

        public BoxDetectionEngine(IEnumerable<IImageProcessor> imageProcessors, IBoxProcessor boxProcessor)
        {
            _imageProcessors = imageProcessors;
            _boxProcessor = boxProcessor;
            _detectedBoxes = new List<Bitmap>();
        }

        public IEnumerable<Bitmap> GetMatchingBoxes(IImage image)
        {
            foreach (var imageProcessor in _imageProcessors)
            {
                var boxes = Process(image, imageProcessor);
                _detectedBoxes.AddRange(boxes);
            }

            return _detectedBoxes;
        }

        public IEnumerable<Bitmap> Process(IImage image, IImageProcessor imageProcessor)
        {
            using (var imgTemp = new Image<Bgr, Byte>((Bitmap)image.Bitmap.Clone()))
            {
                var imgClear = new Image<Bgr, Byte>((Bitmap)image.Bitmap.Clone());
                imageProcessor.ProcessImage(imgTemp);

                var contours = new VectorOfVectorOfPoint();
                CvInvoke.FindContours(imgTemp, contours, null, RetrType.Tree, ChainApproxMethod.ChainApproxSimple);

                var boxList = new List<Bitmap>();

                for (int i = 0; i < contours.Size; i++)
                {
                    var box = CvInvoke.BoundingRectangle(contours[i]);
                    var boxAspectRatio = (double) box.Width / box.Height;
                    var boxPercentOfImg = (double) (box.Width * box.Height) / (imgTemp.Size.Width * imgTemp.Size.Height) * 100.0;

                    
                    if (!(boxAspectRatio >= 3.0 && boxAspectRatio <= 9.0 && boxPercentOfImg >= 0.5)) continue;

                    imgClear.ROI = box;
                    using (var boxImage = imgClear.Copy())
                    {
                        _boxProcessor.ProcessImage(boxImage);

                        var averageColour = boxImage.GetAverage();
                        if (averageColour.Blue > 80 && averageColour.Green > 80 && averageColour.Red > 80) continue;
                        if (averageColour.Blue < 10 && averageColour.Green < 10 && averageColour.Red < 10) continue;

                        boxList.Add(boxImage.ToBitmap());
                        imgClear.ROI = Rectangle.Empty;
                    }
                }

                return boxList;
            }
        }
    }
}