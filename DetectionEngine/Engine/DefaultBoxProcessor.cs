﻿using System.Drawing;
using DetectionEngine.Interfaces;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;

namespace DetectionEngine.Engine
{
    public class DefaultBoxProcessor : IBoxProcessor
    {
        public void ProcessImage(IImage image)
        {
            CvInvoke.CvtColor(image, image, ColorConversion.Bgr2Gray);
            CvInvoke.Threshold(image, image, 170, 255, ThresholdType.Binary);
            CvInvoke.GaussianBlur(image, image, new Size(), 2);
            CvInvoke.AddWeighted(image, 2, image, 0, 0, image);
            CvInvoke.Erode(image, image, null, new Point(), 1, BorderType.Default, new MCvScalar());
            CvInvoke.CvtColor(image, image, ColorConversion.Gray2Bgr);
        }
    }
}
